# otterly adorable

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y_p9iFYYxKY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This is the place to find my otterly adorable character I did for a stickerpack as well as all the stickers. Enjoy.

## Can I use this rig
Yes, you are allowed, as long as it's non-commercial and you use the same creative commons lisence for republishing, you can use this asset for free.

## How was it made?
You can probably figure out something already by looking at the rig, but I will go more depth in a couple of tutorials on youtube.

## I need moar of this otter!
 - [Here's a Demo of this rig on YouTube](https://www.youtube.com/watch?v=Y_p9iFYYxKY)
 - [There's also a cute animation of it over youtube.](https://www.youtube.com/watch?v=BE8rLzwzImw)
 - [Have a look at the same animation on sketchfab.](https://sketchfab.com/3d-models/otterly-adorable-rigging-showcase-ac9baac90f034a46a1496650caadcd0a)
 - [Get the stickerpack for Telegram!](https://t.me/addstickers/otterly_adorable)
